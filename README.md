# Payment Service

## Introduction

This service is responsible for handling payments and interacting with an external or internal payment gateway in a loosely coupled manner. **For now, it is just a simple stream application that contacts a payment gateway, but can be extended**

## Architecture Diagram

![Architecture Diagram](diagram.png)
