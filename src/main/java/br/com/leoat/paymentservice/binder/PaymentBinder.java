package br.com.leoat.paymentservice.binder;

import br.com.leoat.clientservice.model.avro.Client;
import br.com.leoat.paymentservice.api.PaymentGatewayAPI;
import br.com.leoat.paymentservice.model.PaymentResult;
import br.com.leoat.subscriptionstreamprocessor.model.avro.Subscription;
import br.com.leoat.subscriptionstreamprocessor.model.avro.SubscriptionStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.BiFunction;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class PaymentBinder {

    private final PaymentGatewayAPI paymentGatewayAPI;

    @Bean
    public BiFunction<KStream<String, Subscription>, KTable<String, Client>, KStream<String, Subscription>> process() {
        return (productAvailableStream, clientTable) ->
                productAvailableStream.join(clientTable, (subscription, client) -> {
                    log.info("Starting payment process for the subscription {} and client {}", subscription.getId(), client.getId());

                    PaymentResult result = paymentGatewayAPI.charge(subscription.getCurrentPrice(), client.getBalance());

                    if (result.isSuccess()) {
                        log.info("Payment success with message: {}", result.getMessage());
                    } else {
                        log.info("Payment failure with message: {}", result.getMessage());
                    }

                    subscription.setStatus(SubscriptionStatus.PAID);
                    subscription.setClientEmail(client.getEmail());
                    return subscription;
                }).map((k, v) -> new KeyValue<>(v.getId(), v));
    }

}
