package br.com.leoat.paymentservice.api;

import br.com.leoat.paymentservice.model.PaymentResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "payment-gateway", url = "${app.payment-gateway.url}")
public interface PaymentGatewayAPI {

    @PostMapping("/charge")
    PaymentResult charge(@RequestParam("price") Long price, @RequestParam("balance") Long balance);
}
